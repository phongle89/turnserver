var express = require('express');
var app = express();

app.get('/', function (req, res) {
  var CryptoJS = require("crypto-js");
        //return res.json({date : new Date()});
var unixTimeStamp = parseInt(Date.now() / 1000) + 24 * 3600;
  var name = 'user_' + unixTimeStamp;
          username = [unixTimeStamp, name].join(':');
  var hash = CryptoJS.HmacSHA1(username, "hv-turnserver");
  var credential = CryptoJS.enc.Base64.stringify(hash);
  var iceServers = [
          {
                url: 'stun:127.0.0.1:3478'
          },
          {
                url: 'turn:127.0.0.1:3478?transport=tcp',
                username: username,
                credential: credential
          },
          {
                url: 'turn:127.0.0.1:3478?transport=udp',
                username: username,
                credential: credential
          }
  ];
  return res.json(iceServers);
});
app.get('*', function (req, res) {
  res.sendStatus(404);;
});

app.listen(8080, function () {
  console.log('App listening on port 8080!');
});
